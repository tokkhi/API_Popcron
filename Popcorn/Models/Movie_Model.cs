﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Movie_Model
    {
       

        public class Output_List_Movie
        {
            
            public List<List_Movie> List_Movie { get; set; }
            
        }
       public class Output_Detail_Movie
        {
            public List<Detail> Detail_Movie { get; set; }

        }
        public class List_Movie
        {
            public string Name_Movie { get; set; }
            public string ID_Movie { get; set; }
            public string img { get; set; }
            public string Date { get; set; }
            public string Score_Users { get; set; }
            public string Score_Master { get; set; }
        }
        public class Check_like
        {
            public string ID_Movie { get; set; }
            public string ID_Comment { get; set; }
        }

        public class Search_Movie
        {
            public string Name_Movie { get; set; }
        }
        public class show
        {
            public List<List_Movie> Commingsoon { get; set; }
            public List<List_Movie> Top5_Movie { get; set; }
            public List<News> New_News { get; set; }
        }
        public class News
        {
            public string Name { get; set; }
            public string ID { get; set; }
            public string Url { get; set; }
        }
        public class Detail
        {
            public string ID_Movie { get; set; }
            public string Name_Movie { get; set; }
            public string Date_Movie { get; set; }
            public string detail { get; set; }
            public string img { get; set; }
            public string Studio { get; set; }
            public string Studio_Url { get; set; }
            public string Trialer { get; set; }
            public string ID_Studio { get; set; }
            public string Rate { get; set; }
            public double Score_Master { get; set; }
            public double Score_User { get; set; }
            public List<List_C_D> Caster { get; set; }
            public List<List_D_D> Director { get; set; }
            public List<List_G_D> Genre { get; set; }
            public List<Comment> Comment_User { get; set; }
            public List<Comment> Comment_Master { get; set; }
        }
        public class List_C_D
        {
            public string ID_Caster { get; set;}
            public string Name_Lastname_Caster { get; set; }
            public string Picture_Caster { get; set; }
        }
        public class List_D_D
        {
            public string ID_Director { get; set; }
            public string Name { get; set; }
            public string Picture_Director { get; set; }
        }
        public class List_G_D
        {
            public string ID_Genre { get; set; }
            public string Name_Genre { get; set; }
        }
        public class Comment
        {
            public int ID_Comment { get; set; }
            public string ID_Movie { get; set; }
            public string Name { get; set; }
            public Guid Guid { get; set; }
            public string Comment_Text { get; set; }
            public int Count_Like { get; set; }
            public string img { get; set; }
            public bool check { get; set; }
            public string Type { get; set; }
            public string Date { get; set; }


        }
        public class Input
        {
            public string id { get; set; }
            public Guid guid { get; set; }
        }
    }

    
}