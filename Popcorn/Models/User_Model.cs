﻿using Popcorn.Controllers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class User_Model
    {
        public class Login_input
        {
            public string Email { get; set; }
            public string Password { get; set; }
            public string Device_ID { get; set; }
            public Boolean active { get; set; }

        }
        public class regis_input
        {
            public string Email { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public string type { get; set; }
            public string Phone { get; set; }
            public Boolean active { get; set; }
        }
        public class Output_Login : BaseOutput
        {
            public Token token { get; set; }
            public Boolean? active { get; set; }
            public string Name { get; set; }
            public string img { get; set; }
            public int status { get; set; }
            public string Type { get; set; }


        }
        public class Output_Regis : BaseOutput
        {


        }
        public class Detail
        {
            public string Name { get; set; }
            public Guid Guid { get; set; }
            public string img { get; set; }
            public string Date { get; set; }
            public string Email { get; set; }
           
        }
        public class Comment
        {
            public List<comment> comment { get; set; }
        }

        public class comment
        {
            public int ID { get; set; }
            public string text { get; set; }
            public string ID_Movie { get; set; }
            public string Name_Movie { get; set; }
            public string Date { get; set; }
            public int likes { get; set; }

        }
        public class Edit_input
        {
            public Guid guid { get; set; }
            public string Email { get; set; }
            public string img { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public string mtype { get; set; }
            public string Phone { get; set; }
        }
        public class Edit_input_M
        {
            public Guid ID_User { get; set; }
            public string img { get; set; }
            public string Name { get; set; }
        }
        public class Edit_input_Pass
        {
            public Guid ID_User { get; set; }
            public string Password { get; set; }   
        }

    }
    public class Token
    {
        public string app_id { get; set; }
        public string Guid { get; set; }
        public DateTime exp { get; set; }

    }
}