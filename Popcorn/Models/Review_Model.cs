﻿using Popcorn.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Review_Model
    {
        public class Input_Score
        {
            public Guid ID_User { get; set; }
            public double Score { get; set; }
            public string ID_Movie { get; set; }
        }
        public  class Output_Score : BaseOutput
        {

        }
        public class Input_Comment
        {
            public Guid ID_User { get; set; }
            public string ID_Movie { get; set; }
            public string Comment { get; set; }
        }

        public class Input_Comment_Edit
        {
            public Guid ID_User { get; set; }
            public int ID_Comment { get; set; }
            public string Comment { get; set; }
        }
        public class Input_Like
        {
            public Guid ID_User { get; set; }
            public int ID_Comment { get; set; }
        }
        public class Output_Like : BaseOutput
        {

        }
        public class Output_Comment : BaseOutput
        {

        }
        public class Input_sum
        {
            public string ID_Movie { get; set; }
            public int Score { get; set; }
        }
        public class Input_Delete
        {
            public string ID_Comment { get; set; }
        }
        public class Input_Report
        {
            public string ID_Comment { get; set; }
            public Guid ID_User { get; set; }
        }
    }
}