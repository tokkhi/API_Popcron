﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Notification_Model
    {
        public class input
        {
            public string applicationID { get; set; }
            public string senderId { get; set; }
            public string deviceId { get; set; }
            public notification data { get; set; }
        }
        public class notification
        {
            public string sound { get; set; }
            public string title { get; set; }
            public string body { get; set; }
        }
       


    }
}