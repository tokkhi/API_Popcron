﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Caster_Model
    {
        public class Output_List_Caster
        {
            public List<List_Caster> List_Caster { get; set; }
        }
        public class Output_Detail_Caster
        {
            public List<Detail_Caster> Detail_Caster { get; set; }

        }
        public class Detail_Caster
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string img { get; set; }
            public string Detail { get; set; }
            public string Brithday { get; set; }
            public List<List_M> Movie { get; set; }
        }

        public class List_Caster
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string img { get; set; }
        }
        public class List_M
        {
            public string ID_Movie { get; set; }
            public string Name_Movie { get; set; }
            public string img { get; set; }
            public double Score_Master { get; set; }
            public double Score_User { get; set; }
        }
    }
}