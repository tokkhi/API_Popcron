﻿using Popcorn.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Delete_Model
    {
        public class Input_Movie
        {
            public String ID_Movie { get; set; }
        }
        public class Input_User
        {
            public Guid ID_User { get; set; }
        }
        public class Input_Genre
        {
            public String ID_Genre { get; set; }
        }
        public class Input_Caster
        {
            public String ID_Caster { get; set; }
        }
        public class Input_Director
        {
            public String ID_Director { get; set; }
        }
        public class Input_Studio
        {
            public String ID_Studio { get; set; }
        }
        public class Output : BaseOutput
        {
            
        }
        public class Input_News
        {
            public string ID_News { get; set; }
        }
    }
}