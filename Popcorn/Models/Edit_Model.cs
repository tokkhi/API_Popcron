﻿using Popcorn.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Edit_Model
    {
        public class Input_movie
        {
            public string ID_Movie { get; set; }
            public string Name_Movie { get; set; }
            public DateTime Date_Movie { get; set; }
            public string Detail { get; set; }
            public List<List_Cs_Mo> Caster { get; set; }
            public List<List_Ge_Mo> Genre { get; set; }
            public List<List_Dt_Mo> Director { get; set; }
            public string img { get; set; }
            public string urlTri { get; set; }
            public string studio { get; set; }
            public int Rate { get; set; }
            public Guid ID_User { get; set; }
            public bool Status { get; set; }
        }
        public class Output_Movie : BaseOutput
        {
            public string message { get; set; }
        }
      
        public class Input_caster
        {
            public string ID_caster { get; set; }
            public string Name { get; set; }
            public DateTime BrithDay { get; set; }
            public string PictureUrl { get; set; }
            public string Detail { get; set; }
        }
        public class Input_director
        {
            public string id_director { get; set; }
            public string Name { get; set; }
            public DateTime BrithDay { get; set; }
            public string PictureUrl { get; set; }
            public string Detail { get; set; }
        }
        public class Output_Caster : BaseOutput
        {
            public string message { get; set; }
        }

        public class Input_Genre
        {
            public string ID { get; set; }
            public string Name { get; set; }
        }
        public class Output_Genre : BaseOutput
        {
            public string message { get; set; }
        }

       
        public class Output_director : BaseOutput
        {
            public string message { get; set; }
        }

        public class List_Cs_Mo
        {
            public string id_Cs { get; set; }
            public int status { get; set; }
        }
        public class List_Ge_Mo
        {
            public string id_Ge { get; set; }
        }
        public class List_Dt_Mo
        {
            public string id_Dt { get; set; }
            public int status { get; set; }
        }
        public class Input_Studio
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
        }
        public class Input_User
        {
            public Guid Guid_User { get; set; }
            public string Email { get; set; }
            public string Name { get; set; }
            public string type { get; set; }
            public Boolean active { get; set; }
        }
        public class Output_Studio : BaseOutput
        {
            
        }
        public class Input_News
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
        }
        public class Input_Status
        {
            public string ID_Movie { get; set; }
            public bool Status { get; set; }
        }

    }
}