﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Search
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string img { get; set; }
    }
}