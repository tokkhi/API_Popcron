﻿using Popcorn.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class Center
    {
        public class Input_Gen
        {
            public string code { get; set; }
            public string number { get; set; }
        }
        public class Output_Gen : BaseOutput
        {
            public string code { get; set; }
        }
     
    }

}