﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Models
{
    public class News_Model
    {
        public class output_News
        {
            public List<item> News { get; set; }
        }
        public class item
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string url { get; set; }
            public string date { get; set; }
        }
    }
}