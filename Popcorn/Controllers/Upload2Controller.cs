﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Popcorn.Controllers
{

    public class Upload2Controller : Controller
    {
        [HttpPost]
        public string test(HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                var path = (String)null;
                try
                {
                    if (file != null && file.ContentLength > 0 && file.ContentType == "image/jpeg")
                    {
                        var fileName = Path.GetFileName(file.FileName);
                         path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                        file.SaveAs(path);
                    }
                    else
                    {
                        return "error";
                    }
                }
                catch {
                    return "error";
                }
                
                return path;
            }
            return "null";
        }
    }
}
