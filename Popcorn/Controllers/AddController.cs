﻿using Popcorn.Helper;
using Popcorn.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Add_Model;
using static Popcorn.Models.Center;
using static Popcorn.Helper.Check;

namespace Popcorn.Controllers
{
    public class AddController : Controller
    {
       
        [HttpPost]
        public JsonResult Movie(Input_movie input)
        {
            Input_Gen input_Gen = new Input_Gen();
            var id ="";
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            

            //DataClass.GenerateNumber ID_Number = new DataClass.GenerateNumber();
            Output_Movie output = new Output_Movie();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                
                if (!Check.C_login(input.ID_User)) {
                    output.SetValue(BaseOutput.ResultCode.InvalidUserLogin);
                    return Json(output, JsonRequestBehavior.AllowGet);
                } 
                var result = db.TB_Movies.FirstOrDefault(X => X.Name_Movie == input.Name_Movie);
            

                if (result != null)
                {
                    output.SetValue(BaseOutput.ResultCode.alrady);

                }
                else
                {
                    input_Gen.code = "MV";
                    id = Gen_Center.Gen(input_Gen);
                    if (id == null)
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                    else
                    {
                        dbmovie.ID_Movie = id;
                        dbmovie.ID_Studio = input.studio;
                        dbmovie.ID_Rate = input.Rate;
                        dbmovie.Name_Movie = input.Name_Movie;
                        dbmovie.Picture_Movie = input.img;
                        dbmovie.Date_Movie = input.Date_Movie;
                        dbmovie.Detail_Movie = input.Detail;
                        dbmovie.Trialer = input.urlTri;
                        dbmovie.Score_Master = 0;
                        dbmovie.Score_Users = 0;
                        if ("Admins" == Check.status(input.ID_User).status)
                        {
                            dbmovie.show = true;
                        }
                        else
                        {
                            dbmovie.show = false;
                        }

                        db.TB_Movies.InsertOnSubmit(dbmovie);
                        if (input.Director != null)
                        {
                            foreach (var i in input.Director)
                            {
                                DataClass.TB_Director_Movie dbdm = new DataClass.TB_Director_Movie();

                                dbdm.ID_Director = i.id_Dt;
                                dbdm.ID_Movie = id;
                                dbdm.ID_Status_Director = i.status;
                                db.TB_Director_Movies.InsertOnSubmit(dbdm);



                            }
                        }
                        if (input.Genre != null)
                        {
                            foreach (var i in input.Genre)
                            {
                                DataClass.TB_Genre_Movie dbcm = new DataClass.TB_Genre_Movie();

                                dbcm.ID_Genre = i.id_Ge;
                                dbcm.ID_Movie = id;
                                db.TB_Genre_Movies.InsertOnSubmit(dbcm);


                            }
                        }

                        if (input.Caster != null)
                        {
                            foreach (var i in input.Caster)
                            {
                                DataClass.TB_Cast_Movie dbcm = new DataClass.TB_Cast_Movie();

                                dbcm.ID_Caster = i.id_Cs;
                                dbcm.ID_Movie = id;
                                dbcm.ID_Status_Caster = i.status;
                                db.TB_Cast_Movies.InsertOnSubmit(dbcm);

                            }
                        }
                        


                        try
                        {
                           
                            var m = Update_Movie(input.ID_User, id);
                            if (m)
                            {
                                db.SubmitChanges();
                                output.SetValue(BaseOutput.ResultCode.Success);
                            }
                            else
                            {
                                output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                            }
                            

                        }
                        catch (Exception ex)
                        {
                            output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                            Exception ex2 = ex;
                            while (ex2.InnerException != null)
                            {
                                ex2 = ex2.InnerException;
                            }
                            output.SetValue(BaseOutput.ResultCode.InternalError);
                            Console.WriteLine(ex.InnerException);
                            throw;

                        }

                    }

                }
                return Json(output, JsonRequestBehavior.AllowGet);
            }
        }

        private bool Update_Movie(Guid id_User, string id_Mo)
        {
            DataClass.TB_Update_Movie dbup = new DataClass.TB_Update_Movie();
            Input_Gen input_Gen = new Input_Gen();
            var id = "";
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Movies.Where(u => u.ID_Movie == id_Mo).ToList();
                try
                {
                    var date = DateTime.Now.Date;
                    if (result != null)
                    {
                        input_Gen.code = "UP";
                        id = Gen_Center.Gen(input_Gen);
                        dbup.ID_Update = id;
                        dbup.ID_Movie = id_Mo;
                        dbup.Guid_Users = id_User;
                        dbup.Date_Update = date;
                        db.TB_Update_Movies.InsertOnSubmit(dbup);

                    }
                    else
                    {
                        return (false);
                    }
                    db.SubmitChanges();

                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                   
                    Console.WriteLine(ex.InnerException);
                    throw;
                }


            }

            return (true);
        }
        [HttpPost]
        public JsonResult Caster(Input_caster input)
        {

           
            DataClass.TB_Caster dbcs = new DataClass.TB_Caster();
            Input_Gen input_gen = new Input_Gen();
            Output_Caster output = new Output_Caster();
            var id = "";
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Casters.FirstOrDefault(X => X.Name_Lastname_Caster == input.Name);

                if (result != null)
                {
                    output.SetValue(BaseOutput.ResultCode.alrady);

                }
                else
                {

                    input_gen.code = "CT";
                    id = Gen_Center.Gen(input_gen);
                    dbcs.ID_Caster = id;
                    dbcs.Name_Lastname_Caster = input.Name;
                    dbcs.Picture_Caster = input.PictureUrl;
                    dbcs.Detail_Caster = input.Detail;
                    dbcs.Brithday_Caster = input.BrithDay;
                    db.TB_Casters.InsertOnSubmit(dbcs);

                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Genre(Input_Genre input)
        {
            DataClass.TB_Genre dbge = new DataClass.TB_Genre();
            Input_Gen input_gen = new Input_Gen();
            var id = "";
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Genres.FirstOrDefault(X => X.Name_Genre == input.Name);

                if (result != null)
                {
                    output.SetValue(BaseOutput.ResultCode.alrady);

                }
                else
                {

                    input_gen.code = "GE";
                    id = Gen_Center.Gen(input_gen);
                    dbge.ID_Genre = id;
                    dbge.Name_Genre = input.Name;
                   
                    db.TB_Genres.InsertOnSubmit(dbge);

                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Director(Input_director input)
        {
            DataClass.TB_Director dbdt = new DataClass.TB_Director();
            Input_Gen input_gen = new Input_Gen();
            var id = "";
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Directors.FirstOrDefault(X => X.Name_Lastname_Director == input.Name);

                if (result != null)
                {
                    output.SetValue(BaseOutput.ResultCode.alrady);

                }
                else
                {

                    input_gen.code = "DT";
                    id = Gen_Center.Gen(input_gen);
                    dbdt.ID_Director = id;
                    dbdt.Name_Lastname_Director = input.Name;
                    dbdt.Picture_Director = input.PictureUrl;
                    dbdt.Detail_Director = input.Detail;
                    dbdt.Brithday_Director = input.BrithDay;

                    db.TB_Directors.InsertOnSubmit(dbdt);

                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Studio(Input_Studio input)
        {
            DataClass.TB_Studio dbsd = new DataClass.TB_Studio();
            Input_Gen input_gen = new Input_Gen();
            var id = "";
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Studios.FirstOrDefault(X => X.Name_Studio == input.Name);

                if (result != null)
                {
                    output.SetValue(BaseOutput.ResultCode.alrady);

                }
                else
                {

                    input_gen.code = "SD";
                    id = Gen_Center.Gen(input_gen);
                    dbsd.ID_Studio = id;
                    dbsd.Name_Studio = input.Name;
                    dbsd.Link_Studio = input.Url;


                    db.TB_Studios.InsertOnSubmit(dbsd);
                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult News(Input_News input)
        {
            DataClass.TB_News dbn = new DataClass.TB_News();
            Input_Gen input_gen = new Input_Gen();
            
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                
              
                    dbn.Name_News = input.Name;
                    dbn.Link_News = input.Url;
                    db.TB_News.InsertOnSubmit(dbn);
                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            
            return Json(output, JsonRequestBehavior.AllowGet);
        }



    }
}
