﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Popcorn.Models;
using System.Configuration;

namespace Popcorn.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public JsonResult Index(User_Model.regis_input input)
        {
            DataClass.TB_User User = new DataClass.TB_User();

            User_Model.Output_Regis output = new User_Model.Output_Regis();


            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {

                var result = db.TB_Users.FirstOrDefault(X => X.Email == input.Email);

                if (result != null)
                {
                    output.SetValue(BaseOutput.ResultCode.alrady);
                }
                else
                {
                    if (input.Email == null)
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                    else
                    {
                        User.Password = input.Password;
                        User.Name_Lastname_Users = input.Name;
                        User.Email = input.Email;
                        User.Date = DateTime.Now.Date;
                        User.ID_Type = input.type;
                        db.TB_Users.InsertOnSubmit(User);
                        db.SubmitChanges();

                        output.SetValue(BaseOutput.ResultCode.Success);
                    }

                }
                return Json(output, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult Edit(User_Model.Edit_input input)
        {
           
            User_Model.Output_Regis output = new User_Model.Output_Regis();


            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Users.FirstOrDefault(X => X.Guid_Users == input.guid);
                if (result != null)
                {
                    
                    result.img = input.img;
                    result.Name_Lastname_Users = input.Name;
                    result.ID_Type = input.mtype;
                    result.Password = input.Password;
                    try
                    {

                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                   
                }
                else
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
               

            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Edit_M(User_Model.Edit_input_M input)
        {

            User_Model.Output_Regis output = new User_Model.Output_Regis();


            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Users.FirstOrDefault(X => X.Guid_Users == input.ID_User);
                if (result != null)
                {

                    result.img = input.img;
                    result.Name_Lastname_Users = input.Name;
                    try
                    {

                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }

                }
                else
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Edit_Password(User_Model.Edit_input_Pass input)
        {

            User_Model.Output_Regis output = new User_Model.Output_Regis();


            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Users.FirstOrDefault(X => X.Guid_Users == input.ID_User);
                if (result != null)
                {

                    result.Password = input.Password;
                    try
                    {

                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }

                }
                else
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

    }
}