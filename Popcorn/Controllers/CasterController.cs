﻿using Popcorn.Helper;
using Popcorn.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Caster_Model;

namespace Popcorn.Controllers
{
    public class CasterController : Controller
    {
        
        public ActionResult Caster_List()
        {
            Output_List_Caster Output_ = new Output_List_Caster();
            Output_.List_Caster = new List<List_Caster>();

            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                Output_.List_Caster = db.Caster_Movies.Select(c => new List_Caster { ID = c.ID_Caster, Name = c.Name_Lastname_Caster, img = c.Picture_Caster }).Distinct().ToList();



            }

            return Json(Output_, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Caster_Detail(String id)
        {
            Output_Detail_Caster Output_ = new Output_Detail_Caster();
            Output_.Detail_Caster = new List<Detail_Caster>();

            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var item = db.Caster_Movies.Where(c => c.ID_Caster == id).Distinct().FirstOrDefault();
                if (item != null)
                {
                    var movie = db.Movies.Where(m => m.ID_Caster == id).Select(m => new List_M { ID_Movie = m.ID_Movie, img = m.Picture_Movie, Name_Movie = m.Name_Movie, Score_Master = m.Score_Master.Value, Score_User = m.Score_Users.Value }).Distinct().ToList();

                    Output_.Detail_Caster.Add(new Detail_Caster
                    {
                        ID = item.ID_Caster,
                        Name = item.Name_Lastname_Caster,
                        img = item.Picture_Caster,
                        Brithday = GetValue.DateToString(item.Brithday_Caster),
                        Detail = item.Detail_Caster,
                        Movie = movie
                    });

                }

            }

            return Json(Output_, JsonRequestBehavior.AllowGet);
        }
     
    }
}