﻿using Popcorn.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Delete_Model;

namespace Popcorn.Models
{
    public class DeleteController : Controller
    {
        // GET: Delete
        public JsonResult Movie(Input_Movie input)
        {
            Output output = new Output();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var Movie = db.TB_Movies.Where(x => x.ID_Movie == input.ID_Movie).FirstOrDefault();
                var caster = db.TB_Cast_Movies.Where(x => x.ID_Movie == input.ID_Movie);
                var Genre = db.TB_Genre_Movies.Where(x => x.ID_Movie == input.ID_Movie);

                try
                {
                    if (Movie != null)
                    {
                        db.TB_Genre_Movies.DeleteAllOnSubmit(Genre);
                        db.TB_Cast_Movies.DeleteAllOnSubmit(caster);
                        db.TB_Movies.DeleteOnSubmit(Movie);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;

              
                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Users(Input_User input)
        {
            Output output = new Output();
            
            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Users.Where(x => x.Guid_Users == input.ID_User).FirstOrDefault();
                try
                {
                    if (result != null)
                    {
                        db.TB_Users.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {

                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;

                    
                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Genre(Input_Genre input)
        {
            Output output = new Output();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Genres.Where(x => x.ID_Genre == input.ID_Genre).FirstOrDefault();
                try
                {
                    if (result != null)
                    {
                        db.TB_Genres.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;
                    
                }


            }
            return Json(output , JsonRequestBehavior.AllowGet);
        }

        public JsonResult Caster(Input_Caster input)
        {
            Output output = new Output();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Casters.Where(x => x.ID_Caster == input.ID_Caster).FirstOrDefault();
                try
                {
                    if (result != null)
                    {
                        db.TB_Casters.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;
                    
                }


            }
            return Json(output , JsonRequestBehavior.AllowGet);
        }
        public JsonResult Director(Input_Director input)
        {
            Output output = new Output();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Directors.Where(x => x.ID_Director == input.ID_Director).FirstOrDefault();
                try
                {
                    if (result != null)
                    {
                        db.TB_Directors.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;
                    
                }


            }
            return Json(output , JsonRequestBehavior.AllowGet);
        }
        public JsonResult Studio(Input_Studio input)
        {
            Output output = new Output();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Studios.Where(x => x.ID_Studio == input.ID_Studio).FirstOrDefault();
                try
                {
                    if (result != null)
                    {
                        db.TB_Studios.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;
                    
                }


            }
            return Json(output , JsonRequestBehavior.AllowGet);
        }
        public JsonResult News(Input_News input)
        {
            Output output = new Output();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_News.Where(x => x.ID_News == Int32.Parse(input.ID_News)).FirstOrDefault();
                try
                {
                    if (result != null)
                    {
                        db.TB_News.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;

                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult User(Input_User input)
        {
            Output output = new Output();
            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Users.Where(x => x.Guid_Users == input.ID_User).FirstOrDefault();
                try
                {
                    if (result != null)
                    {
                        db.TB_Users.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    else
                    {
                        output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.InternalError);
                    Console.WriteLine(ex.InnerException);
                    throw;

                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
    }
}