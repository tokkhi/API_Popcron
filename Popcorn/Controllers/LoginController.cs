﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Popcorn.Models;
using System.Configuration;
using System.Security;
using System.IdentityModel;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using static Popcorn.Models.User_Model;
using Popcorn.Helper;
using System.Security.Claims;
using Popcorn.DataClass;

namespace Popcorn.Controllers
{
    public class LoginController : Controller
    {

        [HttpPost]
        public JsonResult Index(User_Model.Login_input input)
        {
            User_Model.Output_Login output = new User_Model.Output_Login();


            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {

                var result = db.TB_Users.FirstOrDefault(x => x.Email == input.Email && x.Password == input.Password);
                //var status = result.status.ToString();
                if (result != null)
                {
                    Login_Update(result.Guid_Users,input.Device_ID);
                    output.token = createToken(result.Guid_Users.ToString());
                    output.Type = result.ID_Type;
                    output.Name = result.Name_Lastname_Users;
                    output.img = result.img;
                    output.SetValue(BaseOutput.ResultCode.Success);
                   
                }
                else
                {
                    output.SetValue(BaseOutput.ResultCode.InvalidUserLogin);


                }
                    return Json(output, JsonRequestBehavior.AllowGet);

                }

            }

        [HttpPost]
        public JsonResult Logout(Guid ID_User)
        {
            User_Model.Output_Login output = new User_Model.Output_Login();


            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {

                var result = db.TB_Logins.FirstOrDefault(x => x.Guid_Users == ID_User);
                //var status = result.status.ToString();
                if (result != null)
                {
                   
                    try
                    {
                        db.TB_Logins.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
                else
                {
                    output.SetValue(BaseOutput.ResultCode.InvalidUserLogin);


                }
                return Json(output, JsonRequestBehavior.AllowGet);

            }

        }

        private void Login_Update(Guid guid,string device)
        {

            TB_Login dbl = new TB_Login();
            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                
                var result = db.TB_Logins.FirstOrDefault(X => X.Guid_Users == guid);
                if (result != null)
                {
                    result.ID_Device = device;
                    
                }
                else
                {
                    dbl.Guid_Users = guid;
                    dbl.ID_Device = device;
                    db.TB_Logins.InsertOnSubmit(dbl);
                }
                try
                {
                    db.SubmitChanges();
                    

                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    Console.WriteLine(ex.InnerException);
                    throw;

                    // Provide for exceptions.
                }

            }
        }

        [HttpPost]
        public JsonResult Admin(User_Model.Login_input input)
        {
            User_Model.Output_Login output = new User_Model.Output_Login();


            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {

                var result = db.TB_Users.Where(x => x.ID_Type == "1").FirstOrDefault(x => x.Email == input.Email && x.Password == input.Password);
                //var status = result.status.ToString();
                if (result != null)
                {

                    output.token = createToken(result.Guid_Users.ToString());
                    output.Type = result.ID_Type;
                    output.Name = result.Name_Lastname_Users;
                    output.img = result.img;
                    output.SetValue(BaseOutput.ResultCode.Success);

                }
                else
                {
                    output.SetValue(BaseOutput.ResultCode.InvalidUserLogin);


                }
                return Json(output, JsonRequestBehavior.AllowGet);

            }

        }

        private Token createToken(string username)
        {
            string appkey = ConfigurationManager.AppSettings["appkey"];
            Token token = new Token();
            token.app_id = appkey;
            token.exp = DateTime.UtcNow;
            token.Guid = username;


            return token;
        }


        [HttpPost]
        public JsonResult Users(Guid ID_User)
        {
            Detail output = new Detail();
            
            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Users.Where(c => c.Guid_Users == ID_User).FirstOrDefault();
                var com = db.Comment_users.Where(C => C.Guid_Users == ID_User).Select(x => new comment { ID = x.ID_Comments, Date = GetValue.DateToString(x.Time_Comments), ID_Movie = x.ID_Movie, likes = x.Count_likes.Value, text = x.Comment_Text, Name_Movie = x.Name_Movie }).ToList();

                if (result != null)
                {
                    output.Name = result.Name_Lastname_Users;
                    output.Email = result.Email;
                    output.Date = GetValue.DateToString(result.Date);
                    output.Guid = result.Guid_Users;
                    output.img = result.img;
                }




            }

                return Json(output, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Comment_User(Guid ID_User)
        {
            Comment output = new Comment();

            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Users.Where(c => c.Guid_Users == ID_User).FirstOrDefault();
                var com = db.Comment_users.Where(C => C.Guid_Users == ID_User).Select(x => new comment { ID = x.ID_Comments, Date = GetValue.DateToString(x.Time_Comments), ID_Movie = x.ID_Movie, likes = x.Count_likes.Value, text = x.Comment_Text, Name_Movie = x.Name_Movie }).ToList();

                if (result != null)
                {
                    output.comment = com;
                }




            }

            return Json(output, JsonRequestBehavior.AllowGet);
        }

    }

    }
