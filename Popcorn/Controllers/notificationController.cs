﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static Popcorn.Models.Notification_Model;

namespace Popcorn.Controllers
{
    public class notificationController : Controller
    {
        [HttpPost]
        public void SendPushNotification(input input)
        {
            try

            {
                //string applicationID = "AAAASuucldU:APA91bHE8v_x3m5iYixjkSsb7dCa09rylFawJut5cirEPo_xGJQ9qzrAf7yD2IbUTN3KyJX99dahCjErfUzo6nNRbyNguIrEoZ2lhuo_RL2a0yzpugGZUf934FU41Fu1gYwPTxxTcb6l";
                //string senderId = "321780487637";
                //string deviceId = "f9m2DPyKbGk:APA91bFgiwt2p4HMyuGBgcqk8zj369h4yrTP8N0GHaGTqgPrSS0nP8vBDrwzC6k2_0xwgEpj-sscqFdqiLJOjoOH7AmIm3knOmTCcCh712PJht-T_UoOulRpFV0D1kkNzFxE7m-d9qkn";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = input.deviceId,
                    notification = new
                    {
                        body = input.data.body,
                        title = input.data.title,
                        sound = input.data.sound
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", input.applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", input.senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }

    }
    }
