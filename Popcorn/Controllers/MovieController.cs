﻿using Popcorn.Helper;
using Popcorn.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Movie_Model;

namespace Popcorn.Controllers
{
    public class MovieController : Controller
    {


        public JsonResult Movie_Detail(Input input)
        {
           var id_user = input.guid;

            Output_Detail_Movie Output_m = new Output_Detail_Movie();
            Output_m.Detail_Movie = new List<Detail>();
           
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();

            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var item = (from c in db.Movies where c.ID_Movie == input.id select new { c.ID_Movie, c.Name_Movie, c.Date_Movie, c.Detail_Movie, c.ID_Rate, c.Link_Studio, c.ID_Studio, c.Name_Studio, c.Picture_Movie, c.Score_Master, c.Score_Users, c.Trialer,c.Rate }).Distinct().FirstOrDefault();
                if (item != null)
                {   
                    var list_caster = db.Movies.Where(c => c.ID_Movie == item.ID_Movie).Select( c => new Movie_Model.List_C_D { ID_Caster = c.ID_Caster,Name_Lastname_Caster = c.Name_Lastname_Caster, Picture_Caster = c.Picture_Caster }).Distinct().ToList();
 
                    var list_director = db.Movies.Where(c => c.ID_Movie == item.ID_Movie).Select(c => new Movie_Model.List_D_D { ID_Director = c.ID_Director, Name = c.Name_Lastname_Director, Picture_Director = c.Picture_Director }).Distinct().ToList();

                    var list_genre = db.Movies.Where(c => c.ID_Movie == item.ID_Movie).Select(c => new Movie_Model.List_G_D { Name_Genre = c.Name_Genre,ID_Genre=c.ID_Genre}).Distinct().ToList();

                    var com_us = db.Comments.Where(c => c.ID_Movie == item.ID_Movie && c.ID_Type == "2").Select(c => new Movie_Model.Comment { ID_Comment = c.ID_Comments, ID_Movie = c.ID_Movie, Name = c.Name_Lastname_Users, Guid = c.Guid_Users.Value, Count_Like = c.Count_likes.Value, Comment_Text = c.Comment_Text}).Distinct().ToList();

                    var com_ms = db.Comments.Where(c => c.ID_Movie == item.ID_Movie && c.ID_Type == "3").Select(c => new Movie_Model.Comment { ID_Comment = c.ID_Comments, ID_Movie = c.ID_Movie, Name = c.Name_Lastname_Users, Guid = c.Guid_Users.Value, Count_Like = c.Count_likes.Value, Comment_Text = c.Comment_Text }).Distinct().ToList();


                    List<Comment> comment_User = new List<Comment>();
                        foreach (var item2 in com_us)
                        {
                       
                        var any = db.Likes.Where(x => x.ID_Comments == item2.ID_Comment).Any(x =>x.Guid_Users == input.guid);



                        comment_User.Add(new Comment
                            {
                                ID_Comment = item2.ID_Comment,
                                ID_Movie = item2.ID_Movie,
                                Guid = item2.Guid,
                                Count_Like = item2.Count_Like,
                                Name = item2.Name,
                                img = item2.img,
                                Comment_Text = item2.Comment_Text,
                                check = any
                                
                                

                            });
                        }

                    List<Comment> comment_Master = new List<Comment>();
                    foreach (var item2 in com_ms)
                    {

                        var any = db.Likes.Where(x => x.ID_Comments == item2.ID_Comment).Any(x => x.Guid_Users == input.guid);



                        comment_Master.Add(new Comment
                        {
                            ID_Comment = item2.ID_Comment,
                            ID_Movie = item2.ID_Movie,
                            Guid = item2.Guid,
                            Count_Like = item2.Count_Like,
                            Name = item2.Name,
                            img = item2.img,
                            Comment_Text = item2.Comment_Text,
                            check = any
                            , Date = item2.Date

                        });
                    }



                    Output_m.Detail_Movie.Add(new Detail
                    {
                        ID_Movie = item.ID_Movie,
                        Name_Movie = item.Name_Movie,
                        detail = item.Detail_Movie,
                        Studio = item.Name_Studio,
                        Caster = list_caster,
                        Director = list_director,
                        Genre = list_genre,
                        Rate = item.Rate,
                        img = item.Picture_Movie,
                        ID_Studio = item.ID_Studio,
                        Studio_Url = item.Link_Studio,
                        Trialer = item.Trialer,
                        Date_Movie = GetValue.DateToString(item.Date_Movie),
                        Score_Master = item.Score_Master.Value,
                        Score_User = item.Score_Users.Value,
                        Comment_User = comment_User,
                        Comment_Master = comment_Master
                    });

                    
                }


            }
            return Json(Output_m, JsonRequestBehavior.AllowGet);
        }


        public JsonResult Movie_List()
        {
            Output_List_Movie Output_m = new Output_List_Movie();
            Output_m.List_Movie = new List<List_Movie>();
            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.Movies.Select(m => new { m.Name_Movie, m.Date_Movie, m.ID_Movie, m.Detail_Movie, m.Picture_Movie, m.Score_Master,m.Score_Users }).Distinct();
                if (result != null)
                {
                    foreach (var item in result.AsParallel())
                    {
                       
                        Output_m.List_Movie.Add(new List_Movie
                        {
                            ID_Movie = item.ID_Movie,
                            Name_Movie = item.Name_Movie,
                            img = item.Picture_Movie,
                            Score_Users = item.Score_Users.ToString(),
                            Score_Master = item.Score_Master.ToString(),

                        });
                    }
                }

            }


            return Json(Output_m, JsonRequestBehavior.AllowGet);
        }

       
        public JsonResult Genre_Movie(string input)
        {

            Output_List_Movie Output_m = new Output_List_Movie();
            Output_m.List_Movie = new List<List_Movie>();
            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.Movies.Select(m => new { m.Name_Movie, m.Date_Movie, m.ID_Movie, m.Detail_Movie, m.Picture_Movie, m.Score_Master, m.Score_Users,m.ID_Genre }).Where(m => m.ID_Genre == input).Distinct();
                if (result != null)
                {
                    foreach (var item in result.AsParallel())
                    {

                        Output_m.List_Movie.Add(new List_Movie
                        {
                            ID_Movie = item.ID_Movie,
                            Name_Movie = item.Name_Movie,
                            img = item.Picture_Movie,
                            Score_Users = item.Score_Users.ToString(),
                            Score_Master = item.Score_Master.ToString(),

                        });
                    }
                }

            }


            return Json(Output_m, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Rateing_Movie(string input)
        {

            Output_List_Movie Output_m = new Output_List_Movie();
            Output_m.List_Movie = new List<List_Movie>();
            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.Movies.Select(m => new { m.Name_Movie, m.Date_Movie, m.ID_Movie, m.Detail_Movie, m.Picture_Movie, m.Score_Master, m.Score_Users, m.ID_Rate }).Where(m => m.ID_Rate.ToString() == input).Distinct();
                if (result != null)
                {
                    foreach (var item in result.AsParallel())
                    {

                        Output_m.List_Movie.Add(new List_Movie
                        {
                            ID_Movie = item.ID_Movie,
                            Name_Movie = item.Name_Movie,
                            img = item.Picture_Movie,
                            Score_Users = item.Score_Users.ToString(),
                            Score_Master = item.Score_Master.ToString(),

                        });
                    }
                }

            }


            return Json(Output_m, JsonRequestBehavior.AllowGet);
        }
    }

    }
