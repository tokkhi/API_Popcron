﻿using Popcorn.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Popcorn.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        public JsonResult Index()
        {
            List<Search> output = new List<Search>();
            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                output = db.searches.Select(s => new Search { ID = s.ID_Movie,Name = s.Name_Movie, Status =s.tag }).ToList();
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
    }
}