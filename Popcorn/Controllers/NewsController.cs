﻿using Popcorn.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.News_Model;

namespace Popcorn.Controllers
{
    public class NewsController : Controller
    {
        public JsonResult News()
        {
            output_News Output_ = new output_News();
            Output_.News = new List<item>();

            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                Output_.News = db.News.Select(n => new item { ID = n.ID_News, Name = n.Name_News, url= n.Link_News }).Distinct().ToList();

            }

            return Json(Output_, JsonRequestBehavior.AllowGet);
        }
    }
}