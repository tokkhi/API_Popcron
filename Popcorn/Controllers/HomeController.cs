﻿using Popcorn.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Movie_Model;

namespace Popcorn.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult Show()
        {
            show output = new show();
            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                try {

                    output.Top5_Movie = db.HIT_Movies.OrderByDescending(x => x.Score_Master).Select(m => new List_Movie { ID_Movie = m.ID_Movie, Name_Movie = m.Name_Movie, img = m.Picture_Movie, Score_Master = m.Score_Master.Value.ToString(), Score_Users = m.Score_Users.Value.ToString(),Date = GetValue.DateToString(m.Date_Movie)}).ToList();


                    output.Commingsoon = db.CommingSoons.Select(m => new List_Movie { ID_Movie = m.ID_Movie, Name_Movie = m.Name_Movie, img = m.Picture_Movie}).Distinct().ToList();

                    output.New_News = db.News.Select(n => new News { ID = n.ID_News.ToString(), Name = n.Name_News, Url = n.Link_News }).ToList();

                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    
                    Console.WriteLine(ex.InnerException);
                    throw;

                }

            }


            return Json(output, JsonRequestBehavior.AllowGet);
        }
    }
}