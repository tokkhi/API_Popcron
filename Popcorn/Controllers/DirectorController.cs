﻿using Popcorn.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Director_Model;

namespace Popcorn.Controllers
{
    public class DirectorController : Controller
    {
        public ActionResult Director_List()
        {
            Output_List_Director Output_ = new Output_List_Director();
            Output_.List_Director = new List<List_Director>();

            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                Output_.List_Director = db.Director_Movies.Select(c => new List_Director { ID = c.ID_Director, Name = c.Name_Lastname_Director, img = c.Picture_Director }).Distinct().ToList();
            }

            return Json(Output_, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Director_Detail(String id)
        {
            Output_Detail_Director Output_ = new Output_Detail_Director();
            Output_.Detail_Director = new List<Detail_Director>();
            
            using (DataClass.ViewMovieDataContext db = new DataClass.ViewMovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                if (ModelState.IsValid) // It's valid even when user = null
                {
                    var item = db.Director_Movies.Where(c => c.ID_Director == id).Distinct().FirstOrDefault();
                    if (item != null)
                    {
                        var movie = db.Movies.Where(m => m.ID_Director == id).Select(m => new List_M { ID_Movie = m.ID_Movie, img = m.Picture_Movie, Name_Movie = m.Name_Movie, Score_Master = m.Score_Master.Value, Score_User = m.Score_Users.Value }).Distinct().ToList();

                       

                        Output_.Detail_Director.Add(new Detail_Director
                        {
                            ID = item.ID_Director,
                            Name = item.Name_Lastname_Director,
                            img = item.Picture_Director,
                            Brithday = GetValue.DateToString(item.Brithday_Director),
                            Detail = item.Detail_Director,
                            Movie = movie
                        });

                    }

                }

                return Json(Output_, JsonRequestBehavior.AllowGet);
            }
        }
    }
}