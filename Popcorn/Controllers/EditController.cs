﻿using Popcorn.DataClass;
using Popcorn.Helper;
using Popcorn.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Center;
using static Popcorn.Models.Edit_Model;
using static Popcorn.Models.Notification_Model;

namespace Popcorn.Controllers
{
    public class EditController : Controller
    {
        [HttpPost]
        public JsonResult Movie(Input_movie input)
        {

            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            Output_Movie output = new Output_Movie();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Movies.FirstOrDefault(X => X.ID_Movie == input.ID_Movie);
               

                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);

                }
                else
                {

                    result.ID_Studio = input.studio;
                    result.ID_Rate = input.Rate;
                    result.Name_Movie = input.Name_Movie;
                    result.Picture_Movie = input.img;
                    result.show = result.show;
                    result.Date_Movie = input.Date_Movie;
                    result.Detail_Movie = input.Detail;
                    result.Trialer = input.urlTri;
                    var caster = db.TB_Cast_Movies.Where(c => c.ID_Movie == input.ID_Movie);
                    db.TB_Cast_Movies.DeleteAllOnSubmit(caster);
                    if (input.Caster !=null)
                    {
                        
                        foreach (var i in input.Caster)
                        {
                            DataClass.TB_Cast_Movie dbcm = new DataClass.TB_Cast_Movie();
                            dbcm.ID_Caster = i.id_Cs;
                            dbcm.ID_Status_Caster = i.status;
                            dbcm.ID_Movie = input.ID_Movie;
                            db.TB_Cast_Movies.InsertOnSubmit(dbcm);
                        }
                    }
                    var director = db.TB_Director_Movies.Where(c => c.ID_Movie == input.ID_Movie);
                    db.TB_Director_Movies.DeleteAllOnSubmit(director);
                    if (input.Director != null)
                    {
                        
                      
                        foreach (var i in input.Director)
                        {
                            DataClass.TB_Director_Movie dbdt = new DataClass.TB_Director_Movie();
                            dbdt.ID_Director = i.id_Dt;
                            dbdt.ID_Status_Director = i.status;
                            dbdt.ID_Movie = input.ID_Movie;
                            db.TB_Director_Movies.InsertOnSubmit(dbdt);
                        }
                       
                    }
                    var genre = db.TB_Genre_Movies.Where(c => c.ID_Movie == input.ID_Movie);
                    db.TB_Genre_Movies.DeleteAllOnSubmit(genre);
                    if (input.Genre != null)
                    {
                       
                        foreach (var i in input.Genre)
                        {
                            DataClass.TB_Genre_Movie dbg = new DataClass.TB_Genre_Movie();
                            dbg.ID_Genre = i.id_Ge;
                            dbg.ID_Movie = input.ID_Movie;
                            db.TB_Genre_Movies.InsertOnSubmit(dbg);
                        }
                    }
                    try
                    {
                        var c = Update_Movie(input.ID_User, input.ID_Movie);
                        if (c)
                        {
                            db.SubmitChanges();
                            output.SetValue(BaseOutput.ResultCode.Success);
                        }
                        output.SetValue(BaseOutput.ResultCode.Wrong);





                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;

                        }
                        output.SetValue(BaseOutput.ResultCode.Wrong);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }

                }
                return Json(output, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Caster(Input_caster input)
        {
            DataClass.TB_Caster dbcs = new DataClass.TB_Caster();
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Casters.FirstOrDefault(X => X.ID_Caster == input.ID_caster);

                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);

                }
                else
                {

                    result.Name_Lastname_Caster = input.Name;
                    result.Picture_Caster = input.PictureUrl;
                    result.Detail_Caster = input.Detail;
                    result.Brithday_Caster = input.BrithDay;

                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Director(Input_director input)
        {
            DataClass.TB_Director dbdt = new DataClass.TB_Director();
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Directors.FirstOrDefault(X => X.ID_Director == input.id_director);

                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);

                }
                else
                {

                    result.Name_Lastname_Director = input.Name;
                    result.Picture_Director = input.PictureUrl;
                    result.Detail_Director = input.Detail;
                    result.Brithday_Director = input.BrithDay;
                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Studio(Input_Studio input)
        {
            DataClass.TB_Studio dbsd = new DataClass.TB_Studio();
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Studios.FirstOrDefault(X => X.ID_Studio == input.ID);

                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
                else
                {
                    result.Name_Studio = input.Name;
                    result.Link_Studio = input.Url;
                    
                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Genre(Input_Genre input)
        {
            DataClass.TB_Genre dbg = new DataClass.TB_Genre();
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Genres.FirstOrDefault(X => X.ID_Genre == input.ID);

                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
                else
                {
                    result.Name_Genre = input.Name;
                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult News(Input_News input)
        {
            DataClass.TB_News dbn = new DataClass.TB_News();
            Output_Caster output = new Output_Caster();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_News.FirstOrDefault(X => X.ID_News.ToString() == input.ID);

                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
                else
                {
                    result.Name_News = input.Name;
                    result.Link_News = input.Url;

                    try
                    {
                       
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        private bool Update_Movie(Guid id_User, string id_Mo)
        {
            DataClass.TB_Update_Movie dbup = new DataClass.TB_Update_Movie();
            Input_Gen input_Gen = new Input_Gen();
            var id = "";
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Movies.Where(u => u.ID_Movie == id_Mo).ToList();
                try
                {
                    var date = DateTime.Now.Date;
                    if (result != null)
                    {
                        input_Gen.code = "UP";
                        id = Gen_Center.Gen(input_Gen);
                        dbup.ID_Update = id;
                        dbup.ID_Movie = id_Mo;
                        dbup.Guid_Users = id_User;
                        dbup.Date_Update = date;
                        db.TB_Update_Movies.InsertOnSubmit(dbup);

                    }
                    else
                    {
                        return (false);
                    }
                    db.SubmitChanges();

                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
           
                    Console.WriteLine(ex.InnerException);
                    throw;
                }


            }

            return (true);
        }
        public JsonResult Status_Movie(Input_Status input)
        {
            Output_Caster output = new Output_Caster();
        
            Notification_Model.input noti = new Notification_Model.input();
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Movies.FirstOrDefault(X => X.ID_Movie == input.ID_Movie);

                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
                else
                {
                   
                    var up = db.TB_Update_Movies.Where(c => c.ID_Movie == result.ID_Movie).Select(x => new { x.Guid_Users,x.ID_Update }).OrderByDescending(p => p.ID_Update).First();
                    var log = db.TB_Logins.Where(c => c.Guid_Users == up.Guid_Users).Select(x => new{ID = x.ID_Device }).FirstOrDefault();
                    if (log.ID != null)
                    {
                        noti.deviceId = log.ID;
                        noti.data = new notification();
                        noti.data.body = "ภาพยนตร์ ที่คุณเพิ่มเข้าไป ได้รับ การยืนยันแล้ว";
                        Notification.SendPushNotification(noti);
                    }

                  
                    result.show = input.Status;
                    try
                    {
                        db.SubmitChanges();
                        
                        output.SetValue(BaseOutput.ResultCode.Success);

                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.InternalError);
                        Console.WriteLine(ex.InnerException);
                        throw;
                    }
                }
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

    }
}