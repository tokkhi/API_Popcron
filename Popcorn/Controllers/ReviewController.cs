﻿using Popcorn.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Review_Model;

namespace Popcorn.Controllers
{
    public class ReviewController : Controller
    {
        [HttpPost]
        public JsonResult score_User(Input_Score Input)
        {
            Output_Score output = new Output_Score();
            DataClass.TB_Review_User dbrv = new DataClass.TB_Review_User();
            using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Review_Users.Where(x => x.Guid_Users == Input.ID_User && x.ID_Movie == Input.ID_Movie).FirstOrDefault();

                if (result != null)
                {
                    result.Review_Users = Input.Score;

                }
                else
                {
                    dbrv.Guid_Users = Input.ID_User;
                    dbrv.ID_Movie = Input.ID_Movie;
                    dbrv.Review_Users = Input.Score;
                    db.TB_Review_Users.InsertOnSubmit(dbrv);
                }

                try
                {
                    db.SubmitChanges();
                    sum(Input.ID_Movie, "User");
                    output.SetValue(BaseOutput.ResultCode.Success);

                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.Wrong);
                    Console.WriteLine(ex.InnerException);
                    throw;

                    // Provide for exceptions.
                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public void sum(String input,string type)
        {
            
            using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                Double total ;
                var Movie = db.TB_Movies.Where(r => r.ID_Movie == input).FirstOrDefault();
                if (Movie != null)
                {
                    if (type == "User")
                    {
                        var result = db.TB_Review_Users.Where(r => r.ID_Movie == input);
                        var Count = result.Count();
                        var sum_score = result.Sum(x => x.Review_Users);
                        var score = System.Convert.ToDouble(sum_score);
                        total = (score / Count);
                        total = Math.Round(total, 1);

                        Movie.Score_Users = total;

                    }
                    if (type == "Master")
                    {
                        var result = db.TB_Review_Masters.Where(r => r.ID_Movie == input);
                        var Count = result.Count();
                        var sum_score = result.Sum(x => x.Review_Master);
                        var score = System.Convert.ToDouble(sum_score);
                        total = (score / Count);
                        total = Math.Round(total, 1);

                        Movie.Score_Master = total;
                    }
                    else
                    {

                    }
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }

                }
                else
                {

                }

            }
        }
        [HttpPost]
        public JsonResult score_Master(Input_Score Input)
        {
            Output_Score output = new Output_Score();

            DataClass.TB_Review_Master dbrv = new DataClass.TB_Review_Master();
            using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Review_Masters.Where(x => x.Guid_Users == Input.ID_User && x.ID_Movie == Input.ID_Movie).FirstOrDefault();

                if (result != null)
                {
                    result.Review_Master = Input.Score;

                }
                else
                {

                    dbrv.Guid_Users = Input.ID_User;
                    dbrv.ID_Movie = Input.ID_Movie;
                    dbrv.Review_Master = Input.Score;

                    db.TB_Review_Masters.InsertOnSubmit(dbrv);

                }

                try
                {
                    db.SubmitChanges();
                    sum(Input.ID_Movie, "Master");
                    output.SetValue(BaseOutput.ResultCode.Success);

                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.Wrong);
                    Console.WriteLine(ex.InnerException);
                    throw;

                    // Provide for exceptions.
                }


            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Comment(Input_Comment Input, Token token)
        {
            
            var id = ConfigurationManager.AppSettings["appkey"];
            Output_Comment output = new Output_Comment();
            if (token.app_id != id)
            {

                output.SetValue(BaseOutput.ResultCode.alrady);
            }
            else
            {
                DataClass.TB_Comment dbc = new DataClass.TB_Comment();
                using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
                {
                    var result = db.TB_Comments.Where(x => x.Guid_Users == Input.ID_User && x.ID_Movie == Input.ID_Movie).FirstOrDefault();
                    if (result != null)
                    {
                        output.SetValue(BaseOutput.ResultCode.alrady);
                    }
                    else
                    {
                        dbc.Time_Comments = DateTime.Now.Date;
                        dbc.Guid_Users = Input.ID_User;
                        dbc.ID_Movie = Input.ID_Movie;
                        dbc.Comment_Text = Input.Comment;

                        db.TB_Comments.InsertOnSubmit(dbc);
                        try
                        {
                            db.SubmitChanges();
                            output.SetValue(BaseOutput.ResultCode.Success);


                        }
                        catch (Exception ex)
                        {
                            Exception ex2 = ex;
                            while (ex2.InnerException != null)
                            {
                                ex2 = ex2.InnerException;
                            }
                            output.SetValue(BaseOutput.ResultCode.Wrong);
                            Console.WriteLine(ex.InnerException);
                            throw;

                            // Provide for exceptions.
                        }
                    }

                }
            }
       
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Edit_Comment(Input_Comment_Edit Input)
        {
            Output_Comment output = new Output_Comment();
            DataClass.TB_Comment dbc = new DataClass.TB_Comment();
            using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Comments.Where(x => x.Guid_Users == Input.ID_User && x.ID_Comments == Input.ID_Comment).FirstOrDefault();
                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
                else
                {
                    result.Comment_Text = Input.Comment;

                    try
                    {
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.Wrong);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }

            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete_Comment(Input_Delete Input)
        {
            Output_Comment output = new Output_Comment();
            DataClass.TB_Comment dbc = new DataClass.TB_Comment();
            using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Comments.Where(x => x.ID_Comments.ToString() == Input.ID_Comment).FirstOrDefault();
                var report = db.TB_Reports.Where(x => x.ID_Comments.ToString() == Input.ID_Comment);
                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
                else
                {
                    

                    try
                    {
                        if (report.Any())
                        {
                            db.TB_Reports.DeleteAllOnSubmit(report);
                        }
                        
                        db.TB_Comments.DeleteOnSubmit(result);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.Wrong);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }

            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Report_Comment(Input_Comment_Edit Input)
        {
            Output_Comment output = new Output_Comment();
            DataClass.TB_Report dbr = new DataClass.TB_Report();
            using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Comments.Where(x => x.ID_Comments == Input.ID_Comment).FirstOrDefault();
                if (result == null)
                {
                    output.SetValue(BaseOutput.ResultCode.JsonFormatError);
                }
                else
                {
                    dbr.ID_Comments = Input.ID_Comment;
                    dbr.Guid_Users = Input.ID_User;


                    try
                    {

                        db.TB_Reports.InsertOnSubmit(dbr);
                        db.SubmitChanges();
                        output.SetValue(BaseOutput.ResultCode.Success);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        while (ex2.InnerException != null)
                        {
                            ex2 = ex2.InnerException;
                        }
                        output.SetValue(BaseOutput.ResultCode.Wrong);
                        Console.WriteLine(ex.InnerException);
                        throw;

                        // Provide for exceptions.
                    }
                }

            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Like(Input_Like Input)
        {
            Output_Like output = new Output_Like();
            DataClass.TB_Like dbl = new DataClass.TB_Like();
            using (DataClass.ReviewDataContext db = new DataClass.ReviewDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var result = db.TB_Likes.Where(x => x.Guid_Users == Input.ID_User && x.ID_Comments == Input.ID_Comment).FirstOrDefault();

                if (result != null)
                {
                    db.TB_Likes.DeleteOnSubmit(result);
                }
                else
                {
                    dbl.ID_Comments = Input.ID_Comment;
                    dbl.Guid_Users = Input.ID_User;
                    db.TB_Likes.InsertOnSubmit(dbl);

                }

                try
                {
                    db.SubmitChanges();
                    output.SetValue(BaseOutput.ResultCode.Success);
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    output.SetValue(BaseOutput.ResultCode.Wrong);
                    Console.WriteLine(ex.InnerException);
                    throw;

                    // Provide for exceptions.
                }

            }


            return Json(output, JsonRequestBehavior.AllowGet);
        }
    }
}