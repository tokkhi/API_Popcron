﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Popcorn.Models.Center;

namespace Popcorn.Helper
{
    public class Gen_Center
    {
        public static string Gen(Input_Gen Input)
        {
            
            var strSeqNo = string.Empty;
            var intLength = 5;
            using (DataClass.Data_MovieDataContext db = new DataClass.Data_MovieDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {

                var result = db.Gens.Where(u => u.Name_key == Input.code).FirstOrDefault();
                Input.code = result.Name_key;
                Input.number = result.ID_key.ToString();
                result.ID_key = result.ID_key + 1;


                // Submit the changes to the database.
                try
                {

                    db.SubmitChanges();
                    strSeqNo = string.Format("{0}_{1}", Input.code.Trim(), Input.number.PadLeft(intLength, '0'));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    // Provide for exceptions.
                }

            }
            return (strSeqNo);
        }


    }
}