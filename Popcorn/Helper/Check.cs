﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using static Popcorn.Controllers.Check_Model;

namespace Popcorn.Helper
{
    public static class Check
    {
        public static bool C_login(Guid input)
        {
            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var check = db.TB_Users.Where(u => u.Guid_Users == input).FirstOrDefault();
                if (check.Equals(null))
                {
                    return false;
                }
            }
            return true;

        }
        public static Output status(Guid input)
        {
            Output output = new Output();
            using (DataClass.Data_UsersDataContext db = new DataClass.Data_UsersDataContext(ConfigurationManager.AppSettings["dbConnPopcorn"]))
            {
                var check = db.Type_Users.Where(u => u.Guid_Users == input).FirstOrDefault();

                output.status = check.Name_Type;
            }

            return output;
        }

        internal static void Login()
        {
            throw new NotImplementedException();
        }
    }
}