﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Popcorn.Helper
{
    public class GetValue
    {
        public static string DateToString(DateTime? Input)
        {

            string output = null;
            if (Input.HasValue)
            {
                output = Input.Value.ToString("dd/MM/yyyy");
            }
            return output;

        }

    }
}